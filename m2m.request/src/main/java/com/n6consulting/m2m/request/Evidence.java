package com.n6consulting.m2m.request;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Evidence {
  @Id
  @GeneratedValue
  private Long evidenceId;

  @JsonIgnore
  @ManyToOne
  private Request request;

  private String link;

  // Needed because "text" is a reserved word in SQL
  @Column(name="`text`")
  private String text;

  private Evidence() { }

  public Evidence(final Request request, final String link, final String text) {
    this.request = request;
    this.link = link;
    this.text = text;
  }

  public Long getEvidenceId() {
    return evidenceId;
  }

  public Request getRequest() {
    return request;
  }

  public String getLink() {
    return link;
  }

  public String getText() {
    return text;
  }

  public void setRequest(Request request) {
    this.request = request;
  }
}
